package com.dmytrodryl.Collection.AppliancesTask;

import java.util.ArrayList;

public class AppliancesArray {
    static ArrayList<AppliancesAbstract> appliancesArray = new ArrayList<>();
    static int generalPower = 0;

    public static void calculatePower() {
        appliancesArray.forEach(appliance -> {
            if (appliance.isTurnedOn()) {
                generalPower = generalPower + appliance.getPower();
            }
        });
        System.out.println("General power = " + generalPower);
    }

    public static void sortByPower() {
        appliancesArray.sort(new PowerComparator());
        System.out.println("Sort by Power: " + appliancesArray);
    }

    public static void searchByCriteria(int power) {
        ArrayList<AppliancesAbstract> result = new ArrayList<>();
        appliancesArray.forEach(appliance -> {
            if (appliance.getPower() < power) {
                result.add(appliance);
            }
        });
        System.out.println("Search by Power: " + result);
    }

    public static void searchByCriteria(boolean isTurnedOn) {
        ArrayList<AppliancesAbstract> result = new ArrayList<>();
        appliancesArray.forEach(appliance -> {
            if (appliance.isTurnedOn() == isTurnedOn) {
                result.add(appliance);
            }
        });
        System.out.println("Search by turnedOn: " + result);
    }

    public static void searchByCriteria(String name) {
        ArrayList<AppliancesAbstract> result = new ArrayList<>();
        appliancesArray.forEach(appliance -> {
            if (appliance.getName() == name) {
                result.add(appliance);
            }
        });
        System.out.println("Search by name: " + result);
    }
}
