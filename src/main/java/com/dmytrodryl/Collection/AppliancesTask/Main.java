package com.dmytrodryl.Collection.AppliancesTask;

import com.dmytrodryl.Collection.AppliancesTask.Appliances.*;

public class Main {
    public static void main(String[] args) {
        Fridge fridge = new Fridge(55);
        Boiler boiler = new Boiler(80);
        Dryer dryer = new Dryer(500, true);
        Iron iron = new Iron(500, false);
        Laptop laptop = new Laptop(200, false);
        TV tv = new TV(50);
        dryer.setTurnedOn(false);
        laptop.setTurnedOn(true);
        Laptop laptop1 = new Laptop(140, false);
        AppliancesArray.calculatePower();
        System.out.println(AppliancesArray.appliancesArray);
        AppliancesArray.sortByPower();
        AppliancesArray.searchByCriteria(200);
        AppliancesArray.searchByCriteria(false);
        AppliancesArray.searchByCriteria("Laptop");

    }
}
