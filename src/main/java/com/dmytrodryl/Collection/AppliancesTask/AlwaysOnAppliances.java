package com.dmytrodryl.Collection.AppliancesTask;

public abstract class AlwaysOnAppliances extends AppliancesAbstract {
    public AlwaysOnAppliances(int power) {
        super(power);
        super.setTurnedOn(true);
    }
}
