package com.dmytrodryl.Collection.AppliancesTask;

public abstract class AppliancesAbstract{
   private String name;
   private int power;
   private boolean isTurnedOn;

    public void setName(String name) {
        this.name = name;
    }

    public AppliancesAbstract(int power) {
        this.power = power;
        AppliancesArray.appliancesArray.add(this);
    }

    public AppliancesAbstract(int power, boolean isTurnedOn) {
        this.power = power;
        this.isTurnedOn = isTurnedOn;
        AppliancesArray.appliancesArray.add(this);
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    public void setTurnedOn(boolean turnedOn) {
        isTurnedOn = turnedOn;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "{name='" + name + '\'' +
                ", power=" + power +
                ", isTurnedOn=" + isTurnedOn +
                '}';
    }
}
