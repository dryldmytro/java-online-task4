package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AppliancesAbstract;

public class Dryer extends AppliancesAbstract {
    public Dryer(int power, boolean isTurnedOn) {
        super(power, isTurnedOn);
        setName("Dryer");
    }
}
