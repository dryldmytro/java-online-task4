package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AppliancesAbstract;

public class Laptop extends AppliancesAbstract {
    public Laptop(int power, boolean isTurnedOn) {
        super(power, isTurnedOn);
        setName("Laptop");
    }
}
