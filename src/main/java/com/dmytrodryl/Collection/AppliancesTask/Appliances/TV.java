package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AlwaysOnAppliances;

public class TV extends AlwaysOnAppliances {

    public TV(int power) {
        super(power);
        setName("TV");
    }
}
