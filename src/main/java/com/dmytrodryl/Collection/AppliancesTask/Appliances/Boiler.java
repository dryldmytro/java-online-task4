package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AlwaysOnAppliances;

public class Boiler extends AlwaysOnAppliances {
    public Boiler(int power) {
        super(power);
        setName("Boiler");
    }
}
