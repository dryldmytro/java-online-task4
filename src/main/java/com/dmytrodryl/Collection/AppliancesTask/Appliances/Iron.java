package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AppliancesAbstract;

public class Iron extends AppliancesAbstract {
    public Iron(int power, boolean isTurnedOn) {
        super(power, isTurnedOn);
        setName("Iron");
    }
}
