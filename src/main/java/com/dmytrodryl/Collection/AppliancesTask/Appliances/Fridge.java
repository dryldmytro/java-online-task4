package com.dmytrodryl.Collection.AppliancesTask.Appliances;

import com.dmytrodryl.Collection.AppliancesTask.AlwaysOnAppliances;

public class Fridge extends AlwaysOnAppliances {
    public Fridge(int power) {
        super(power);
        setName("Fridge");
    }
}
