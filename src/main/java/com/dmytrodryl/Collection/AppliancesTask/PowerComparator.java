package com.dmytrodryl.Collection.AppliancesTask;

import java.util.Comparator;

public class PowerComparator implements Comparator<AppliancesAbstract> {
    @Override
    public int compare(AppliancesAbstract o1, AppliancesAbstract o2) {
        return o1.getPower() - o2.getPower();
    }
}
