package com.dmytrodryl.Collection.Container;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        long startTimeMyContainer = System.nanoTime();
        MyContainer myContainer = new MyContainer();
        myContainer.addElement("1 element");
        myContainer.addElement("2 element");
        myContainer.addElement("3 element");
        myContainer.addElement("4 element");
        myContainer.addElement("5 element");
        myContainer.addElement("6 element");
        myContainer.addElement("7 element");
        myContainer.addElement("8 element");
        myContainer.addElement("9 element");
        myContainer.addElement("10 element");
        long estimatedTimeMyContainer = System.nanoTime()-startTimeMyContainer;
        System.out.println("Speed myContainer = "+estimatedTimeMyContainer+" nanosec");

        long startTimeArrayList = System.nanoTime();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1 element");
        arrayList.add("2 element");
        arrayList.add("3 element");
        arrayList.add("4 element");
        arrayList.add("5 element");
        arrayList.add("6 element");
        arrayList.add("7 element");
        arrayList.add("8 element");
        arrayList.add("9 element");
        arrayList.add("10 element");
        long estimatedTimeArrayList = System.nanoTime() - startTimeArrayList;
        System.out.println("Speed ArrayList = "+estimatedTimeArrayList+" nanosec");
    }
}
