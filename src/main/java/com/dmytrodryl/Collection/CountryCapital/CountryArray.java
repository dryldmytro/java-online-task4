package com.dmytrodryl.Collection.CountryCapital;

import java.util.ArrayList;

public class CountryArray {
    private ArrayList<Country> countries = new ArrayList<>();

    public CountryArray() {
        fillArray();
    }

    public ArrayList<Country> getCountries() {
        return countries;
    }
    public void fillArray(){
        Country france = new Country("France", "Paris");
        Country denmark = new Country("Denmark","Copenhagen");
        Country england = new Country("Englang", "London");
        Country ireland = new Country("Ireland", "Dublin");
        Country poland = new Country("Poland", "Warsaw");
        countries.add(france);
        countries.add(denmark);
        countries.add(england);
        countries.add(poland);
        countries.add(ireland);
    }
}
