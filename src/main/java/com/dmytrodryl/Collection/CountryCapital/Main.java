package com.dmytrodryl.Collection.CountryCapital;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {
        ArrayList<Country> arrayList = new CountryArray().getCountries();
        int arraySize = arrayList.size();
        Country[] countries = new Country[arraySize];
        for (int i = 0; i < countries.length; i++) {
            countries[i] = arrayList.get(i);
        }
        Country countryForSearch = new Country("France", "Paris");

 /* sort arrayList by Country and binarySearch
        System.out.print("Not sorted ArrayList: ");
        arrayList.forEach(country -> System.out.print(country));
        System.out.println();
        Collections.sort(arrayList);
        arrayList.forEach(country -> System.out.print(country));
        System.out.println();
        int index = Collections.binarySearch(arrayList,countryForSearch);
        System.out.println(index+" "+arrayList.get(index));*/

/*  sort countries by Country and binarySearch
        System.out.print("Not sorted Array: ");
        for (Country c : countries) {
            System.out.print(c);
        }
        System.out.println();
        Arrays.sort(countries);
        for (Country c : countries) {
            System.out.print(c);
        }
        System.out.println();
        int index = Arrays.binarySearch(countries,countryForSearch);
        System.out.println(index+" "+countries[index]);*/
        CapitalComparator capitalComparator = new CapitalComparator();
      /* sort arrayList by Capital and binarySearch
       System.out.print("Sorted ArrayList: ");
        arrayList.sort(capitalComparator);
        arrayList.forEach(country -> System.out.print(country));
        System.out.println();
        int index = Collections.binarySearch(arrayList,countryForSearch);
        System.out.println(index+" "+arrayList.get(index));
        */
        System.out.print("Sorted Array: ");
        Arrays.sort(countries, capitalComparator);
        for (Country c : countries) {
            System.out.print(c);
        }
        System.out.println();
        int index = Arrays.binarySearch(countries,countryForSearch);
        System.out.println(index+" "+countries[index]);
    }
}
