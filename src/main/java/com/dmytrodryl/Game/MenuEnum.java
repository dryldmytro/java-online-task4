package com.dmytrodryl.Game;

import java.util.ArrayList;

public enum MenuEnum {
    OPEN_THE_DOOR("Open the door."), GET_DOORS("Who is behind the doors?"),
    WHERE_DEATH("Where does the hero expect death?"),GET_WAY("Get the way to win.") , EXIT;
    private String menuName;

    MenuEnum() {
    }

    MenuEnum(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuName() {
        if (menuName == null) {
            return menuName = this.name();
        } else return menuName;
    }

    public static void initializeEnum() {
        for (MenuEnum m : MenuEnum.values()) {
            System.out.println(m.ordinal() + 1 + ". " + m.getMenuName());
        }
    }

    public static void userChoiсe(int userChoiсe) {
        switch (userChoiсe) {
            case 1:Game.openDoor();
                break;
            case 2: Doors.getList();
            Game.start();
                break;
            case 3: int x = Game.whereExpectDeath(0,Doors.doors.size()-1);
                System.out.println("Behind "+x+" doors monsters are stronger than you!");
                Game.start();
                break;
            case 4:
                Game.initialMap();
                System.out.print("You can win with this way - ");
                System.out.println(Game.getWinPath(new ArrayList<>()));
                Game.start();
                break;
            case 5:System.out.println("Bye!");
                System.exit(0);break;
            default:
                System.out.println("Menu with number "+ userChoiсe+" is missing.");
                Game.start();
                break;
        }
    }
}
