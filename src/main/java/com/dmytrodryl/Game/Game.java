package com.dmytrodryl.Game;

import java.util.*;

public class Game {
    private static Scanner scanner = new Scanner(System.in);
    static String characterName = scanner.nextLine();
    static Hero hero = new Hero(characterName);
    static HashMap<Integer, Integer> map = new HashMap<>();

    public static void getHero() {
        System.out.println("Hi, " + hero.getName() + "! Your strength now is: " + hero.getStrength());
        Doors.initializeDoors();
    }

    public static void start() {
        System.out.println("Please make your choice.");
        MenuEnum.initializeEnum();
        int userChoice = scanner.nextInt();
        MenuEnum.userChoiсe(userChoice);
    }

    public static void openDoor() {
        if (Doors.doors.size() >= 1) {
            System.out.println("You can choose a door from 1 to " + Doors.doors.size() + ".\nWhat doors do you want to open?");
            int door = scanner.nextInt() - 1;
            if (door>=10 || door<0){
                System.out.println("Try other door.");
                openDoor();
            }
            int number = Doors.getNumberStrength(door, Doors.doors);
            if (number > 0) {
                hero.setStrength(hero.getStrength() + number);
                System.out.println("Artefact add you +" + number + "\nNow your power is " + hero.getStrength());
                Doors.doors.remove(door);
                if (Doors.doors.size() == 0) win();
                System.out.println("Now you can choose a door from 1 to " + Doors.doors.size() + ".");
                start();
            }
            if (number < 0) {
                if (hero.getStrength() + number >= 0) {
                    System.out.println("You win this figth");
                    Doors.doors.remove(door);
                    if (Doors.doors.size() == 0) win();
                    System.out.println("Now you can choose a door from 1 to " + Doors.doors.size() + ".");
                    start();
                } else {
                    System.out.println("Sorry, you lose.");
                    lose();
                }
            }
        }
    }

    public static void lose() {
        System.out.println("Play again? 1 - Yes, 2 - No");
        int playOrExit = scanner.nextInt();
        if (playOrExit == 1) {
            hero.setStrength(25);
            Doors.doors.clear();
            Doors.initializeDoors();
            start();
        } else if (playOrExit == 2) {
            System.out.println("Bye!");
            System.exit(0);
        } else {
            System.out.println("Please, make your choice.");
            lose();
        }
    }

    public static void win() {
        System.out.println("Congratulations. You won the game.");
        lose();
    }

    public static int whereExpectDeath(int result, int i) {
        if (i < 0) {
            return result;
        }
        int x = Doors.getNumberStrength(i, Doors.doors);
        if (x + hero.getStrength() < 0) {
            ++result;
        }
        return whereExpectDeath(result, --i);
    }
    public static void initialMap(){
        for (int i = 0; i < Doors.doors.size(); i++) {
            map.put(i + 1, Doors.getNumberStrength(i, Doors.doors));
        }

    }

    public static ArrayList<Integer> getWinPath(ArrayList<Integer> arr) {
        Iterator<Integer> iterator = map.keySet().iterator();
        int i = 0;
        if (i>10){
            System.out.println("You cant win this game!");
        }
        if (map.size()<1){
            hero.setStrength(25);
            return arr;
        }
        if (map.size() >= 1) {
            while (iterator.hasNext()) {
                int count = iterator.next();
                if (map.get(count) >= 0) {
                    hero.setStrength(hero.getStrength() + map.get(count));
                    arr.add(count);
                    iterator.remove();
                } else {
                    if (map.get(count) + hero.getStrength() > 0) {
                        arr.add(count);
                        iterator.remove();
                    }
                }
            }
         i++;
        }
        return getWinPath(arr);
    }
}