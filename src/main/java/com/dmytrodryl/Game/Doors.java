package com.dmytrodryl.Game;

import java.util.ArrayList;
import java.util.Random;

public class Doors {
    static ArrayList<Object> doors = new ArrayList<Object>(10);
    public static void initializeDoors(){
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int randomNumber = random.nextInt(2);
            if (randomNumber==0){
                Monster monster = new Monster();
                doors.add(monster);
            }
            if (randomNumber==1){
                Artifact artifact = new Artifact();
                doors.add(artifact);
            }
        }
    }
    public static void getList(){
        for (int i=0;i<doors.size();i++) {
            Object o = doors.get(i);
            int a = i+1;
            System.out.print("In the "+a+" doors are - ");
            if (o.getClass()==Monster.class){
                Monster monster = (Monster)o;
                System.out.println(monster.toString()+". Strength: "+monster.getStrengthMonster());
            }
            if (o.getClass()==Artifact.class){
                Artifact artifact = (Artifact)o;
                System.out.println(artifact.toString()+". Power: +"+artifact.artifactPower);
            }
        }
    }
    public static int getNumberStrength(int doorNumber, ArrayList<Object>arr) {
        Object o = arr.get(doorNumber);
        if (o.getClass() == Artifact.class) {
            Artifact artifact = (Artifact) o;
            int doorStrength = artifact.artifactPower;
            return doorStrength;
        }
        if (o.getClass()==Monster.class){
            Monster monster = (Monster) o;
            int doorStrength = - monster.getStrengthMonster();
            return doorStrength;
        }else return 0;
    }
}