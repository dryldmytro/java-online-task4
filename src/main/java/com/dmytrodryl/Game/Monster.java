package com.dmytrodryl.Game;

import java.util.Random;

public class Monster {
    Random random = new Random();
    private int strengthMonster =5+random.nextInt(100-5);

    public int getStrengthMonster() {
        return strengthMonster;
    }

    @Override
    public String toString() {
        return "Monster";
    }
}
