package com.dmytrodryl.Arrays;

import java.util.Arrays;

public class MainArray {
    public static int[] returnArray(int[] a) {
        int[] c = new int[a.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.out.println(Arrays.toString(c));
        return c;
    }

    public static int[] returnArrayOfTwoArrays(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        System.out.println(Arrays.toString(c));
        return c;
    }

    public static int[] returnArrayWithoutRepeat(int[] a) {
        Arrays.sort(a);
        int firstElementOfOldArray = a[0];
        int sizeOfNewArray = 1;
        for (int i = 1; i < a.length; i++) {
            if (firstElementOfOldArray != a[i]) {
                sizeOfNewArray++;
                firstElementOfOldArray = a[i];
            }
        }
        int newArray[] = new int[sizeOfNewArray];
        firstElementOfOldArray = a[0];
        int firstElementOfNewArray = 0;
        newArray[0] = firstElementOfOldArray;
        for (int i = 1; i < a.length; i++) {
            if (firstElementOfOldArray != a[i]) {
                newArray[firstElementOfNewArray + 1] = a[i];
                firstElementOfNewArray++;
                firstElementOfOldArray = a[i];
            }
        }
        System.out.println(Arrays.toString(newArray));
        return newArray;

    }

    public static int[] deleteElementsRepeatingTwice(int[] a) {
        int arrayLength = 0;
        for (int i : a) {
            int numberOfRepeat = 0;
            for (int j = 0; j < a.length; j++) {
                if (i == a[j]) {
                    numberOfRepeat++;
                }
            }
            if (numberOfRepeat <= 2) {
                arrayLength++;
            }
            numberOfRepeat = 0;
        }
        int newArray[] = new int[arrayLength];
        int countElementsNewArray = 0;
        for (int i : a) {
            int numberOfRepeat = 0;
            for (int j = 0; j < a.length; j++) {
                if (i == a[j]) {
                    numberOfRepeat++;
                }
            }
            if (numberOfRepeat <= 2) {
                arrayLength++;
                newArray[countElementsNewArray] = i;
                countElementsNewArray++;
            }
            numberOfRepeat = 0;
        }
        System.out.println(Arrays.toString(newArray));
        return newArray;
    }

}
