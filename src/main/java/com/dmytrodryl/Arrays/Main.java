package com.dmytrodryl.Arrays;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int [] a = {1,4,6,2,2,5,7,6,8,6};
        int [] b = {2,4,9,5,66,23,1,4,1,6,6,7,6};
        int [] c =MainArray.returnArrayOfTwoArrays(a,b);
        Arrays.sort(c);
        System.out.println(Arrays.toString(c));
        MainArray.returnArrayWithoutRepeat(c);
        MainArray.deleteElementsRepeatingTwice(c);


    }
}
